/**
 * @format
 */
 import React, { Component } from 'react'
import {AppRegistry} from 'react-native';
import App from './Src/Todo/index';
import {name as appName} from './app.json';
import { Provider } from 'react-redux'
import { store } from './Src/Actions/Store/Store'
import { Provider as PaperProvider } from 'react-native-paper';

const RNRedux = () => (
    <Provider store = { store }>
        <PaperProvider>
        <App/>

        </PaperProvider>
     
    </Provider>
  )

AppRegistry.registerComponent(appName, () => RNRedux);
