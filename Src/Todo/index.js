import { Text, View, StyleSheet, FlatList, SafeAreaView, ToastAndroid } from 'react-native'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { doAddoperation, getTodoTextInputValue, AddTodoItem,getUserId } from '../Actions/Action';
import { DoFetchRequest } from '../API/ApiManager';
import { FAB, TextInput as MeterialTextInput, Button, HelperText, Card, Provider as PaperProvider } from 'react-native-paper';
import { ServerUrls } from '../Constants/ServerUrls';
import { homeStyle } from '../Constants/HomeStyeles'
import { ScrollView } from 'react-native-virtualized-view';
import RBSheet from "react-native-raw-bottom-sheet";
import RBSheetUpdate from "react-native-raw-bottom-sheet";
import Icon from 'react-native-vector-icons/Ionicons';
import LoadingView from '../Constants/LoadingView';
export class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todoList: [], selectedId: null, textvalidation: true,textcolorHilte:'red',isLoading:false

    }

  }

  componentDidMount() {
    this.getTodoList()
  }
  AddtodoButton = () => {
  //  console.log("test textinput value", this.props.TodoNewItem)

    this.addTodo()
  }


  addTodo = () => {

    if (this.props.TodoNewItem == '') {
      this.setState({
        textvalidation: true
      })
    } else {
      this.setState({
        textvalidation: false
      })

      this.AddToApiRequest()

    }

  }

  AddToApiRequest = async () => {
    this.setState({
      isLoading: true
    })

    var usersurl = ServerUrls.LOGIN
    const postParms = {
      'title': this.props.TodoNewItem,
      'userId': this.props.TODOUSERID.item.userId,
      'completed': true,

    }

    const getresponse = await DoFetchRequest(usersurl, 'POST', "no", postParms)
    console.log("prames", getresponse)
    if (getresponse.success) {
      this.setState({
        isLoading:false
     })
      ToastAndroid.show("Todo added", ToastAndroid.SHORT);
      this.RBSheet.close()
      this.getTodoList()
    }

  }

  UpdatetodoButton = () => {
    console.log("test textinput value", this.props.TodoNewItem)

    this.UpdateTodo()
  }




  inComplted = () => {
  this.setState({
    textvalidation:false
  
  })
  this.props.AddTodoItem([])
  this.getTodoList()
  }

  
  Complted = () => {
    this.setState({
      textvalidation:true
    })
    this.props.AddTodoItem([])
    this.getTodoList()
    }



  UpdateTodo = () => {

    // this.props.navigation.navigate('DETAILS')
    if (this.props.TodoNewItem == '') {
      this.setState({
        textvalidation: true
      })
    } else {
      this.setState({
        textvalidation: false
      })

      this.UpdateToApiRequest()

    }

  }

  UpdateToApiRequest = async () => {
    this.setState({
      isLoading:true
   })
    var usersurl = ServerUrls.LOGIN +this.props.TODOUSERID.item.id
    const postParms = {
      'title': this.props.TodoNewItem,
      'userId': this.props.TODOUSERID.item.userId,
      'completed': true,

    }

    const getresponse = await DoFetchRequest(usersurl, 'PUT', "no", postParms)
    console.log("prames", getresponse)
    if (getresponse.success) {
      this.setState({
        isLoading:false
     })
      ToastAndroid.show("Todo Updated", ToastAndroid.SHORT);
      this.RBSheetUpdate.close()
      this.getTodoList()
    }

  }




   addtodobtn = (text) =>{

    this.props.getUserId(text)
    this.props.getTodoTextInputValue(text.item.title)
    this.RBSheet.open()
   
   }
   updateToDo = (text) =>{
     try{
      this.props.getTodoTextInputValue(text.item.title)
      this.props.getUserId(text)
      this.RBSheetUpdate.open()
       
     }catch(error){
      console.log(error);
     }
  
   }


   removeToDo = (text) =>{

      try {
    this.props.getUserId(text)
       this.RemoveToApiRequest()
      }
      
      catch(error){
        console.log(error);
      }
   }

   RemoveToApiRequest = async () => {
    try {
      var usersurl = ServerUrls.LOGIN +this.props.TODOUSERID.item.id
        const getresponse = await DoFetchRequest(usersurl, 'DELETE’', "no")
      this.getTodoList()
    }
      catch(error){
        
        console.log(error);
      }
   

  }



  CartTestItemRender = (rowitem) => {
    var backgroundColor = ""
    var textColor = ''

    if (rowitem.item.completed == true) {
      backgroundColor = '#A71C5F'
      textColor = '#ffffff'
    } else {
      backgroundColor = '#FFFFFF'
      textColor = '#000000'
    }

    return (

      <View>


        <View style={homeStyle.slot_main}>
          <Card style={homeStyle.card} >
           
            <View style={homeStyle.cart_card_pading}>
              
            <View style={{flex:1}}>
           
              <Text style={[textColor]}> UserID: {rowitem.item.userId} </Text>
              <Text style={[textColor]}> {rowitem.item.title} </Text>
              <View style={homeStyle.cart_card_pading}>
                <View style={{ flex: 1 }}>
                  <Text style={homeStyle.btn_change} onPress={() => this.addtodobtn(rowitem)}>ADD</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={homeStyle.btn_change}  onPress={() => this.updateToDo(rowitem)}> Update </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={homeStyle.btn_change}  onPress={() => this.removeToDo(rowitem.item.id)}> Remove </Text>
                </View>
              </View>

              
              
            </View>

            {rowitem.item.completed == true?  <Icon name="checkmark-circle" size={45} color="#39e600" />: null }
            {rowitem.item.completed == false?  <Icon name="checkmark-circle" size={45} color="red" />: null }
            
              </View>
           
            
          </Card>
        </View>
      </View>
    )
  }



  getTodoList = async () => {

   this.setState({
                        isLoading:true
                     })

    var usersurl = ServerUrls.LOGIN
     const gettodolistItemdata = await DoFetchRequest(usersurl, 'GET', "no")
     this.setState({
      todoList :[]
     })
   
     this.setState({
      isLoading:false
   })
     this.setState({
      todoList : gettodolistItemdata.response
     })
    

     if(this.state.todoList.length !=0){
      for (let i = 0; i < this.state.todoList.length; i++) {
        var list=[]
        if (this.state.todoList[i].completed == this.state.textvalidation) {
          list=[this.state.todoList[i]]
          this.props.AddTodoItem(this.props.TodoList.concat(list))
        }
       
      }
     }
     
   // this.props.AddTodoItem(gettodolistItem.response)
    
  }

  render() {
      
 

    return (
      <PaperProvider>
        
        <SafeAreaView>
          <View>
            <View style={homeStyle.navigation_bar}>
              <View style={homeStyle.container} >
                <Text style={homeStyle.btn_change_txt}>TODOLIST</Text>
                <View style={homeStyle.cart_card_pading}>

                <View style={{ flex: 1 }}>
                  <Text style={[homeStyle.btn_change_txt,this.state.textcolorHilte]}  onPress={() => this.Complted()}> COMPLETED </Text>
                </View>

                <View style={{ flex: 1 }}>
                  <Text style={homeStyle.btn_change_txt}  onPress={() => this.inComplted()}> INCOMPLETED </Text>
                </View>

                  </View>
              </View>
            </View>
            <ScrollView>
              <FlatList
                data={this.props.TodoList}
                renderItem={this.CartTestItemRender}
                scrollEnabled={false}
                keyExtractor={item => item.id}
                nestedScrollEnabled={false} >
              </FlatList>
            </ScrollView>

          </View>
          <LoadingView show={this.state.isLoading}></LoadingView>

        </SafeAreaView>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          openDuration={250}>
            <View style={{padding:20}}>
            <MeterialTextInput style={{ marginTop: '5%' }}
            placeholder={"Enter Title"} mode='outlined'
            label='Enter Title' error={this.state.mobileError}
            activeOutlineColor="#FBBA00"
            value={this.props.TodoNewItem} onChangeText={(text) => this.props.getTodoTextInputValue(text)}>
          </MeterialTextInput>
          <HelperText type="error" visible={this.state.mobileError}> Enter Text </HelperText>
          <Text style={homeStyle.btn_change} onPress={() => this.AddtodoButton()}> ADD </Text>
            </View>
         
        </RBSheet>

        <RBSheetUpdate
          ref={ref => {
            this.RBSheetUpdate = ref;
          }}
          height={300}
          openDuration={250}>
            <View style={{padding:20}}>
            <MeterialTextInput style={{ marginTop: '5%' }}
            placeholder={"Enter Title"} mode='outlined'
            label='Enter Title' error={this.state.mobileError}
            activeOutlineColor="#FBBA00"
            value={this.props.TodoNewItem} onChangeText={(text) => this.props.getTodoTextInputValue(text)}>
          </MeterialTextInput>
          <HelperText type="error" visible={this.state.mobileError}> Enter Text </HelperText>
          <Text style={homeStyle.btn_change} onPress={() => this.UpdatetodoButton()}> Update </Text>
            </View>
         
        </RBSheetUpdate>
   
      </PaperProvider>

    )
  }
}
const mapStateToProps = state => ({
  TodoReducer: state.TodoValues,
  TodoNewItem: state.TodoValues.TodoNewItem,
  TodoList: state.TodoValues.TodoList,
  TODOUSERID:state.TodoValues.TODOUSERID

});
const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ doAddoperation, getTodoTextInputValue, AddTodoItem,getUserId }, dispatch)

});
export default connect(mapStateToProps, mapDispatchToProps)(index)
