import envs from "../Environment/Envs"
const Baseurl=envs.Baseurl
console.log('endpoint........',Baseurl)

export const BuildRequset = async (url,method,type,body,headers={'Content-Type':'application/json'})=>{

    const endpoint= Baseurl.concat(url)
    console.log('endpoint........',endpoint)
    const reqbody=body?JSON.stringify(body):null
    console.log('reqbody........',reqbody)
    if(type =='no'){
  
      headers={'Content-Type':'application/json','Cache-Control': 'no-cache, no-store, must-revalidate',
      'Pragma': 'no-cache',
      'Expires': 0}
     
    }
    

    
    const fetchParams={method,headers}
    if((method == 'POST' || method=='PUT') && ! reqbody){
        throw new Error("Body is required for POST & PUT requests")
    }
    if (reqbody){
        fetchParams.body = reqbody       
    }
    
    
     console.log('body........',fetchParams)
     try{
       
        const fetchresponse= fetch(endpoint,fetchParams)

        // .then((fetchresponse)=>fetchresponse.json())
        // .then((json)=>{
        //     console.log("test input params",json)
        // })




         const timeoutPromise=new Promise((resolve, reject) => {
            setTimeout(() => {
              resolve('foo');
              reject("Request Timedout")
            }, 30000);
          });
          const response = await Promise.race([fetchresponse , timeoutPromise])
           return fetchresponse    
     }catch(error){
    return error
     }
   
    
   
    
    }  


    export const DoFetchRequest = async (url,method,type,body = null) => {

        try {
            const headers= {}
            const result = {
    
                success : false,
                response : null
            }
            headers['Content-Type'] = 'application/json'
    
            const response= await BuildRequset(url, method,type, body, headers)
            
            if(response.status && response.status == '200' || response.status == '201' ){
              result.success = true
              let json =  await response.json();
              result.response = json
            

              return result
            }
            const errortext = response.message
            return errortext
        } catch(error){
            return error.message

        }
       

    }