import { createStore, combineReducers } from "redux";
import { TodoReducer } from "../Reducers/Reducers";
 const rootReducer = combineReducers({
     TodoValues: TodoReducer,
 })

 export const store = createStore(rootReducer);