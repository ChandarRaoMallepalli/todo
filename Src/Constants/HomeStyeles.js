import {StyleSheet} from 'react-native'
import { StyleConstants } from '../Constants/StyleConstans'

export const homeStyle=StyleSheet.create({
    parentView:{

       alignItems:'center', flexDirection: 'row',margin:'5%'
    },
    card_style:{
      margin:10
   },
    navigation_bar:{

      height:80,backgroundColor:'#A71C5F'
   },

    card:{

      height:130,borderRadius:10,margin:10
   },

   

   btn_change:{
   
    textAlign:'center',
    alignContent:'center',alignItems:'center',alignSelf:'center',
      fontSize: 15,
      fontWeight:'800',
      fontFamily: Platform.OS === "ios" ? "Menlo-Bold" : null,
      color: "#A71C5F",
      textAlign:'center'
      
    },
    cart_card_pading:{
   flexDirection:'row',
   marginTop:10,
   padding:10
     },
     btn_change_txt:{
   
      textAlign:'center',
      alignContent:'center',alignItems:'center',alignSelf:'center',
        fontSize: 15,
        fontWeight:'800',
        fontFamily: Platform.OS === "ios" ? "Menlo-Bold" : null,
        color: "#FFFFFF",
        textAlign:'center'
        
      },
 })