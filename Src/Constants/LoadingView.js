import { Text, View,ActivityIndicator,Modal } from "react-native"
// import { ActivityIndicator } from "react-native-paper"
import React  from 'react'

export default LoadView = (props) =>{

    
    return(
        <Modal animationType="none" transparent={props.show} visible={props.show}>

<View style={{flex:1 , justifyContent : 'center' , alignItems : 'center'}}>

<View>
    <ActivityIndicator size='large' />
   
</View>
  </View>
        </Modal>
       
    )
}